package com.example.springbootbasic.Component_Service_Repository;
import com.example.springbootbasic.Component_Service_Repository.model.Girl;
import com.example.springbootbasic.Component_Service_Repository.service.GirlService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        // thực hiện tạo ra container sau đó sẽ chạy hàm được đánh dấu với
        // annotation @PostConstruct.
        ApplicationContext context = SpringApplication.run(App.class, args);

        GirlService girlService = context.getBean(GirlService.class);
        Girl girl = girlService.getRandomGirl();
        System.out.println(girl);
    }
}
