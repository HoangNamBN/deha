package com.example.springbootbasic.ConfigurationAndBean;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
    @Bean("mysqlConnector")
    DataConnector mysqlConfig(){
        DataConnector mysqlConnector = new MySqlConnector();
        mysqlConnector.setUrl("jdbc:mysql://host1:33060/loda");
        return mysqlConnector;
    }

    @Bean("postgreConnector")
    DataConnector postgreConfig(){
        DataConnector postgreConnector = new MySqlConnector();
        postgreConnector.setUrl("postgresql://localhost/loda");
        return postgreConnector;
    }
}
