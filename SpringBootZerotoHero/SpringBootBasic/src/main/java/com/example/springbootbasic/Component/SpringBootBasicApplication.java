package com.example.springbootbasic.Component;

import com.example.springbootbasic.Autowired.Girl;
import com.example.springbootbasic.Autowired.Outfix;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class SpringBootBasicApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SpringBootBasicApplication.class, args);

        com.example.springbootbasic.Autowired.Outfix outfix = context.getBean(Outfix.class);
        System.out.println("Instance: " + outfix);
        outfix.wear();
    }
}
