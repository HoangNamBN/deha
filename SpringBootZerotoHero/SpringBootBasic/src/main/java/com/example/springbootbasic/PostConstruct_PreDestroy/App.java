package com.example.springbootbasic.PostConstruct_PreDestroy;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class App {
    public static void main(String[] args) {
        // thực hiện tạo ra container sau đó sẽ chạy hàm được đánh dấu với
        // annotation @PostConstruct.
        ApplicationContext context = SpringApplication.run(App.class, args);

        Girl girl = context.getBean(Girl.class);
        ((ConfigurableApplicationContext) context).getBeanFactory().destroyBean(girl);

    }
}
