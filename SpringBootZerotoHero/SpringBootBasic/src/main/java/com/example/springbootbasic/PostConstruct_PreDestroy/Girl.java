package com.example.springbootbasic.PostConstruct_PreDestroy;

import com.example.springbootbasic.AutowiredPrimary.Outfix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Girl {
    @PostConstruct
    public void postConstruct(){
        System.out.println("Object Girl sau khi được khởi tạo sẽ chayj hàm này");
    }
    @PreDestroy
    public void preDestroy(){
        System.out.println("Đối trước Girl trước khi bị Destroy sẽ chạy hàm này");
    }
}
