package com.example.springbootbasic.ConfigurationAndBean;

public abstract class DataConnector {
    private String url;
    public abstract void connect();

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
