package com.example.springbootbasic.Component_Service_Repository.repository;

import com.example.springbootbasic.Component_Service_Repository.model.Girl;
import org.springframework.stereotype.Repository;

@Repository
public class GirlRepositoryImpl implements GirlRepository{
    @Override
    public Girl getGrilByName(String name) {
        /**
         *  trả về một cô gái với tên đúng như tham số
         *  bình thường là phải query trong csdl
         **/
        return new Girl(name);
    }
}
