package com.example.springbootbasic.AutowiredPrimary;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
// thêm Primary để giải quyết vấn đề 2 đối tượng cùng thỏa mãn Outfix
@Primary
// khi thêm @Quafilier thì thêm tên Component
//@Component("bikini")
public class Bikini implements Outfix{
    @Override
    public void wear() {
        System.out.println("Mặc Bikini");
    }
}
