package com.example.springbootbasic.Component_Service_Repository.repository;

import com.example.springbootbasic.Component_Service_Repository.model.Girl;

public interface GirlRepository {
    /**
     * Tìm kiếm một cô gái trong database theo tên
     * @param name
     * @return
     * */
    Girl getGrilByName(String name);
}
