package com.example.springbootbasic.ComponentScan;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Girl {
    @Override
    public String toString() {
        return "Girl.java";
    }
}
