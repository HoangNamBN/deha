package com.example.springbootbasic.AutowiredPrimary;

import org.springframework.stereotype.Component;

@Component
// khi sử dụng @Qualifier thfi thêm tên Component
//@Component("naked")
public class Naked implements Outfix{
    @Override
    public void wear() {
        System.out.println("Không mặc bikini");
    }
}
